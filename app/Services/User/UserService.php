<?php


namespace App\Services\User;


use App\Helpers\ResponseEnum;
use App\Models\User;
use App\Services\BaseService;


class UserService extends BaseService
{
    // 存储用户信息
    public function store(array $params)
    {
        $user = User::query()->create([
            'mobile' => $params['mobile'],
            'password' => bcrypt($params['password']),
        ]);
        if (!$user) {
            $this->throwBusinessException(ResponseEnum::HTTP_ERROR, '注册失败');
        }
    }

}
